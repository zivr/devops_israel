package "nginx"

#template "/etc/nginx/nginx.conf" do
#  source    "rails."
#  notifies  :restart, "service[rails::nginx]"
#end

service "nginx" do
	supports :status => true, :restart => true, :reload => true
  action [:enable, :start]
end